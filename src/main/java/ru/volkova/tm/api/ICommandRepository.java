package ru.volkova.tm.api;

import ru.volkova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
