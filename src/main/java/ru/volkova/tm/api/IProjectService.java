package ru.volkova.tm.api;

import ru.volkova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void clear();

}
