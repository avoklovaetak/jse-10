package ru.volkova.tm.api;

import ru.volkova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
