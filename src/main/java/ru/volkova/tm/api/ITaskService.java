package ru.volkova.tm.api;

import ru.volkova.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void clear();

}
